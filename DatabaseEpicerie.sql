CREATE DATABASE Epicerie;
USE Epicerie;
CREATE TABLE articles
(
    article_id INT PRIMARY KEY NOT NULL,
    name        VARCHAR(20),
    price       INT
);
CREATE TABLE users(
    user_id INT PRIMARY KEY NOT NULL ,
    budget INT
);
CREATE TABLE articles_inventory(
    article_id INT ,
    inventory_id INT ,
    id INT  ,
    quantity INT,
    PRIMARY KEY(article_id,id)
);
USE Epicerie;
CREATE TABLE inventory(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(20),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);