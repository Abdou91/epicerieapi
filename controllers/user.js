const express = require('express');
const router = express.Router();
const models = require('./../models')

router.get('/', async function(req, res) {
const [rows, fields] = await models.user.fetchAll();
  res.format({
    'text/html': function () {
      res.render('index', {
        users: rows,
      })
    },
  
    'application/json': function () {
      res.json(rows)
    },
  })
});

router.get('/:id', async function(req, res) {
  const [rows, fields] = await models.user.fetchOne(req.params.id);
  res.format({
    'text/html': function () {
      res.render('index', {
        users: rows,
      })
    },
  
    'application/json': function () {
      res.json(rows)
    },
  })
});
router.get('/:id/modify_users', async function(req, res) {
  // console.log('test')
   const [rows, fields] = await models.user.fetchOne(req.params.id);
   res.format({
     'text/html': function () {
       res.render('modify_users', {
         users: rows,
       })
     },
   
     'application/json': function () {
       res.json(rows)
     },
   })
 });


router.post('/', async function(req, res) {
  const [rows, fields] = await models.user.add(req.body);
  res.json(rows)
});
router.delete('/:id', async function(req, res) {
  const [rows, fields] = await models.user.delete(req.params.id);
  res.json(rows)
});

 router.get('/:id/delete_users', async function(req, res) {
  // console.log('test')
   const [rows, fields] = await models.user.delete(req.params.id);
   res.format({
     'text/html': function () {
       res.render('delete_users', {
         users: rows,
       })
     },
     'application/json': function () {
       res.json(rows)
     },
   })
 });

 router.post('/:id', async function(req, res) {
  const [rows, fields] = await models.user.modify(req.params.id , req.body);
  console.log("wesh" ,req.body)
  res.format({
    'text/html': function () {
      res.redirect('/users') ;
      },
    'application/json': function () {
      res.json(rows)
    },
  });
})
module.exports = router
