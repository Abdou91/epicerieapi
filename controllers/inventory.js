const express = require('express');
const router = express.Router();
const models = require('./../models')

router.get('/', async function(req, res) {
  const [rows, fields] = await models.inventory.fetchAll();
  res.format({
    'text/html': function () {
      res.render('inventory', {
        inventories: rows,
      })
    },
    'application/json': function () {
      res.json(rows)
    },
  })
});

router.get('/:id', async function(req, res) {
  const [rows, fields] = await models.inventory.fetchOne(req.params.id);
  res.format({
    'text/html': function () {
      res.render('inventory', {
        inventories: rows,
      })
    },
    'application/json': function () {
      res.json(rows)
    },
  })
});

router.delete('/:id', async function(req, res) {
  console.log(req.params.id)
  const [rows, fields] = await models.inventory.delete(req.params.id);
  res.json(rows)
});
router.post('/', async function(req, res) {
  const [rows, fields] = await models.inventory.add(req.body);
  res.json(rows)
});

router.patch('/:id', async function(req, res) {
  const [rows, fields] = await models.inventory.modify(req.body.name, req.params.id);
  res.json(rows)
});

   module.exports = router