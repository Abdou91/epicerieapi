module.exports = {
  fetchAll() {
    return connection.execute('SELECT * FROM users')
  },
  fetchOne(id) {
    if(id)
    {
      return connection.execute('SELECT * FROM `users` WHERE `user_id`=?', [id] )   
    }
    
  },
  delete(id) {
    
    return connection.execute('DELETE  FROM `users` WHERE `user_id`=?', [id])
     
  },

  add(body) {
     return connection.execute('INSERT INTO users(user_id ,budget) VALUES(?,?)',[body.user_id , body.budget])
  },
  modify(id , body) {
    console.log(body)
    return connection.execute(
      'UPDATE users SET user_id=? , budget=?  WHERE user_id=?',[ body.id, body.budget, id])
   
  },
}