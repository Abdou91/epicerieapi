module.exports = {
    fetchAll() {
        return connection.execute('SELECT * FROM inventory')
      },
      fetchOne(id) {
        if(id)
        {
          return connection.execute('SELECT * FROM `inventory` WHERE `id`=?', [id] )   
        }
        
      },
      delete(id) {
        return connection.execute('DELETE  FROM `inventory` WHERE `id`=?', [id]) 
      },
    
      add(body) {
         return connection.execute('INSERT INTO inventory(name, created_at, updated_at) VALUES(?,?,?)',[body.name, new Date() , new Date()])
      },
      modify(name,id) {
        return connection.execute('UPDATE `inventory` SET name = ? WHERE id = ?', [name, id])
      },
}